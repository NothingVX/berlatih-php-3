<?php

require_once 'animal.php';

class Ape extends Animal{
    public $legs = 2;
    public $yell  = "Auooo ";

    public function yell(){
        echo "<br> Name : " . $this->name . "<br>" ; 
        echo "legs : " . $this->legs . "<br>";
        echo "cold blooded : " . $this->cold_blooded . "<br>" ;
        echo "Jump : " . $this->yell . "<br>";
    }
}
?>