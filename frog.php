<?php

require_once 'animal.php';

class Frog extends Animal{
    public $jump  = "Hop Hop ";

    public function jump(){
        
        echo "<br> Name : " . $this->name . "<br>" ; 
        echo "legs : " . $this->legs . "<br>";
        echo "cold blooded : " . $this->cold_blooded . "<br>" ;
        echo "Jump : " . $this->jump . "<br>";
    }
}

?>