<?php
include 'animal.php';
include 'frog.php';
include 'ape.php';

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded . "<br>"; // "no"

// index.php
$sungokong = new Ape("kera sakti");
$sungokong->yell();// "Auooo"

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>